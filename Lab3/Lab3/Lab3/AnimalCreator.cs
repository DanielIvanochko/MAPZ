﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3
{
    internal class AnimalCreator
    {
        private AnimalBuilder animalBuilder;
        public AnimalCreator(AnimalBuilder animalBuilder)
        {
            this.animalBuilder = animalBuilder;
        }
        public void construct()
        {
            animalBuilder.setName();
            animalBuilder.setLegsNumber();
            animalBuilder.setKingdom();
            animalBuilder.setMaxAge();
        }
        public Animal getAnimal()
        {
            return animalBuilder.getAnimal();
        }
    }
}
