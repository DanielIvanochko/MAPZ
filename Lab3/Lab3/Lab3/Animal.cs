﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3
{
    internal class Animal
    {
        private String name;
        private String kingdom;
        private int legsNumber;
        private int maxAge;
        public Animal() {
            this.name = "";
            this.kingdom = "";
            this.maxAge = 0;
            this.legsNumber = 0;
        }
        public Animal(string name, string kingdom, int legsNumber, int maxAge)
        {
            this.name = name;
            this.kingdom = kingdom;
            this.legsNumber = legsNumber;
            this.maxAge = maxAge;
        }
        public String Name { get { return name; } set { name = value; } }
        public String Kingdom { get { return kingdom; } set { kingdom = value; } }
        public int LegsNumber { get { return legsNumber; } set { legsNumber = value; } }
        public int MaxAge { get { return maxAge; } set { maxAge = value; } }
    }
}
