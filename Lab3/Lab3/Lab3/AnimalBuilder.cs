﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3
{
    internal interface AnimalBuilder
    {
        void setName();
        void setMaxAge();
        void setLegsNumber();
        void setKingdom();
        public Animal getAnimal();
    }
}
