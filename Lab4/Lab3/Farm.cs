﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3
{
    internal class Farm
    {
        private String name;
        private String location;
        private static Farm instance;
        private DetachmentFactory detachmentFactory;
        private Farm() { }
        public static Farm getInstance()
        {
            if(instance == null)
            {
                instance = new Farm();
            }
            return instance;
        }
        public String Name { get { return name; } set { name = value; } }
        public String Location { get { return location; } set { location = value; } }
        public void print()
        {
            Console.WriteLine("This farm has name {0} and is located in {1}", name, location);
        }
    }
}
