﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3
{
    internal class HorseDetachment:OuterSpaceDetachment
    {
        private double area ;
        private int maxAnimalsNumber ;
        private String animalType ;
        private String barrierType ;
        private List<Animal> animals;
        public HorseDetachment()
        {
            this.area = 1000;
            this.maxAnimalsNumber = 100;
            this.animalType = "horse";
            this.barrierType = "fence";
            this.animals = new List<Animal>();
        }
        public string getAnimalType()
        {
            return animalType; 
        }

        public double getArea()
        {
            return area;
        }

        public string getBarrierType()
        {
            return barrierType;
        }

        public int getMaxAnimalsNumber()
        {
            return maxAnimalsNumber;
        }
        public void addAnimal(Animal animal)
        {
            this.animals.Add(animal);
        }
    }
}
