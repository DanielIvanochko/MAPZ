﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4
{
    internal class MountainAddon:FieldAddons
    {
        public MountainAddon(Field field):base(field)
        {
        }
        public override String getFieldType()
        {
            return base.getFieldType() + ", mountains";
        }
    }
}
