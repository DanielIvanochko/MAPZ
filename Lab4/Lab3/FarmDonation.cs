﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4
{
    internal class FarmDonation:Donation
    {
        private double currentMoney;
        public FarmDonation()
        {
            this.currentMoney = 0;
        }

        public void addMoney(double money)
        {
            this.currentMoney += money;
        }

        public void reviewMoney()
        {
            Console.WriteLine("Current balance:{0}", currentMoney);
        }

        public bool spendMoney(double money)
        {
            bool result = false;
            if (money <= currentMoney)
            {
                currentMoney -= money;
            }
            return result;
        }
    }
}
