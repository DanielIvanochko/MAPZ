﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    public class Group
    {
        public int id;
        public string name;
        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        public string Name { get { return name; }
            set { name = value; }
        }
        public Group(int id, string name)
        {
            this.id = id;
            this.name = name;
        }
    }
}
