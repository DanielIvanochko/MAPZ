﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    public static class StudentExtension
    {
        public static string getFullName(this Student student)
        {
            return string.Concat(student.LastName + " ", student.FirstName);
        }
    }
}
