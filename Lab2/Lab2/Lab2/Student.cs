﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    public class Student
    {
        private string lastName;
        private string firstName;
        private double age;
        private int id;
        private int groupId;
        public Student() { }
        public Student(string lastName, string firstName, double age, int id, int groupId)
        {
            this.lastName = lastName;
            this.firstName = firstName;
            this.age = age;
            this.id = id;
            this.groupId = groupId;
        }
        public string LastName { get { return lastName; } set { lastName = value; } }
        public string FirstName { get { return firstName; } set { firstName = value; } }
        public double Age { get { return age; } set { age = value; } }
        public int Id { get { return id; } set { id = value; } }
        public int GroupId { get { return groupId; } set { groupId = value; } }
        
    }
}
