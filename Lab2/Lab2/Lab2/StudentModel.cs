﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    public class StudentModel
    {
        public List<Student> students;
        public List<Group> groups;
        public Dictionary<Group,List<Student>> groupsDictionary;
        
        public void createStudents()
        {
            students = new List<Student>();
            students.Add(new Student("Jordan", "Michael", 55, 1, 1));
            students.Add(new Student("Irving", "Kyrie", 25, 2, 2));
            students.Add(new Student("James", "Lebron", 37, 3, 1));
            students.Add(new Student("Harden", "James", 26, 4, 2));
            students.Add(new Student("Parker", "Tony", 40, 5, 1));
            students.Add(new Student("Lillard", "Damian", 30, 6, 2));
            students.Add(new Student("Morant", "Ja", 23, 7, 1));
            students.Add(new Student("Doncic", "Luka", 24, 8, 2));
        }
        public void createGroups()
        {
            groups = new List<Group>();
            groups.Add(new Group(1, "pz-21"));
            groups.Add(new Group(2, "pz-22"));
        }
        public void createDictionary()
        {
            groupsDictionary = new Dictionary<Group, List<Student>>();
        }
        public List<Student> selectStudents(){
            List<Student> someStudents = students.Select(student => new Student(student.LastName, student.FirstName,
                                                        student.Age, student.Id,student.GroupId)).ToList();
            return someStudents;
        }
        public List<Student> selectStudentsWithEvenId()
        {
            List<Student> studentsWithEvenId = (from student in students where student.Id %2==0 select student).ToList();
            return studentsWithEvenId;
        }
        public bool addStudent(Student student)
        {
            bool result = false;
            if (students.Where(s => s.Id == student.Id).FirstOrDefault() == null)
            {
                students.Add(student);
                result = true;
            }
            return result;
        }
        public bool asignStudentsToGroup(int groupId)
        {
            bool result = false;
            Group group = groups.Where(g => g.Id == groupId).FirstOrDefault();
            List<Student> studentWithGroupId = students.Where(student => student.GroupId == groupId).ToList();
            if (group != null&&studentWithGroupId!=null)
            {
                if (groupsDictionary.ContainsKey(group))
                {
                    groupsDictionary[group] = studentWithGroupId;
                }
                else
                {
                    groupsDictionary.Add(group, studentWithGroupId);
                }
                result = true;
            }
            return result;
        }
        public void sortStudentsByName()
        {
            students.Sort(delegate (Student s1,Student s2)
            {
                return s1.getFullName().CompareTo(s2.getFullName());
            });
        }
        public Student[] getStudentsArray()
        {
            return students.ToArray();
        }
        public Student createStudentWithInitializer(String lastName,String firstName, double age, int id, int groupId)
        {
            return new Student { LastName = lastName, FirstName = firstName, Age = age, Id = id, GroupId = groupId }; 
        }
        public void createAnonymousObject(int id,String name)
        {
            var anonymous = new { Id = id, Name = name};
            Console.WriteLine(anonymous.Id +" "+ anonymous.Name);
        }
        public List<Student> groupByGroupId()
        {
            var groupedStudents = students.GroupBy(student=>student.GroupId).ToList();
            List<Student> resultStudents = new List<Student>();
            foreach(var group in groupedStudents)
            {
                foreach(var student in group)
                {
                    resultStudents.Add(student);
                }
            }
            return resultStudents;
        }
        public List<Student> sortStudentsByFunc<T>(Func<Student, T> order)
        {
            return students.OrderBy(order).ToList();
        }
        public void printStudents()
        {
            foreach(var student in students)
            {
                Console.WriteLine(String.Format("Firstname {0}, Lastname {1}, Id {2},  Age {3}, groupid {4}", student.FirstName,
                    student.LastName,student.Id,student.Age,student.GroupId));
            }
        }
    }
}
