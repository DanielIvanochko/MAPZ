using NUnit.Framework;
using Lab2;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace UnitTests
{
    public class Tests
    {
        StudentModel studentModel = new StudentModel();
        [SetUp]
        public void Setup()
        {
            studentModel.createStudents();
            studentModel.createGroups();
            studentModel.createDictionary();
        }

        [Test]
        public void selectStudents()
        {
            List<Student> students = studentModel.selectStudents();
            Assert.IsNotEmpty(students);
        }
        [Test]
        public void selectStudentsWithEvenId()
        {
            List<Student> students = studentModel.selectStudentsWithEvenId();
            foreach(Student student in students)
            {
                Assert.IsTrue(student.Id % 2 == 0);
            }
        }
        [Test]
        public void addStudent()
        {
            Student student = new Student("lastname", "firstname", 15, 10, 2);
            Assert.IsTrue(studentModel.addStudent(student));
        }
        [Test]
        public void addExistingStudent()
        {
            Student student = new Student("Jordan", "Michael", 55, 1, 1);
            Assert.IsFalse(studentModel.addStudent(student));
        }
        [Test]
        public void asignStudentsToGroup() {
            Assert.IsTrue(studentModel.asignStudentsToGroup(1));
        }
        [Test]
        public void asignStudentsToNotExistingGroup()
        {
            Assert.IsFalse(studentModel.asignStudentsToGroup(3));
        }
        [Test]
        public void sortStudentsByName()
        {
            studentModel.sortStudentsByName();
            List<Student> students = studentModel.selectStudents();
            Student[] studentsArray = students.ToArray();
            for(int i = 0; i < studentsArray.Length-1; i++)
            {
                Assert.True(studentsArray[i].getFullName().CompareTo(studentsArray[i + 1].getFullName())<0);
            }
        }
        [Test]
        public void getStudentsArray()
        {
            Student[] studentArray = studentModel.getStudentsArray();
            Assert.IsTrue(studentArray.Length==studentModel.selectStudents().Count);
        }
        [Test]
        public void createStudentWithInitializer()
        {
            Student student = studentModel.createStudentWithInitializer("lastname",
                "firstname", 18, 20, 2);
            Assert.AreEqual(student.LastName, "lastname");
            Assert.AreEqual(student.FirstName, "firstname");
            Assert.AreEqual(student.Age, 18);
            Assert.AreEqual(student.Id, 20);
            Assert.AreEqual(student.GroupId, 2);
        }
        [Test]
        public void groupStudentsByGroupId()
        {
            HashSet<int> groupsSet = new HashSet<int>();
            List<Student> groupedStudents = studentModel.groupByGroupId();
            int currentDistinctGroupId = groupedStudents.First().GroupId;
            groupsSet.Add(currentDistinctGroupId);
            foreach(Student student in groupedStudents)
            {
                int currentGroupId = student.GroupId;
                if (currentGroupId != currentDistinctGroupId){
                    Assert.IsFalse(groupsSet.Contains(currentGroupId));
                    groupsSet.Add(currentGroupId);
                    currentDistinctGroupId = currentGroupId;
                }
            }
        }
        [Test]
        public void sortStudentsByOwnOrder()
        {
            Func<Student,double> order = delegate(Student student) { return student.Age; };
            var students = studentModel.sortStudentsByFunc(order);
            var studentsArray = students.ToArray();
            for (int i = 0; i < studentsArray.Length-1; i++)
            {
                Assert.IsTrue(studentsArray[i].Age < studentsArray[i + 1].Age);
            }
        }
    }
}