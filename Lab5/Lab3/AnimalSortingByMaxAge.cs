﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3
{
    internal class AnimalSortingByMaxAge:AnimalSortingStrategy
    {
        public List<Animal> sort(List<Animal> animals)
        {
            return animals.OrderBy(a => a.MaxAge).ToList();
        }
    }
}
