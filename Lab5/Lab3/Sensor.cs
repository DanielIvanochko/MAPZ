﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4
{
    internal class Sensor: Observer
    {
        private double temperature;
        private double pressure;
        private double humidity;
        private WeatherForecast weatherForecast;
        
        public Sensor(WeatherForecast weatherForecast)
        {
            this.weatherForecast = weatherForecast;   
            weatherForecast.registerObserver(this);
        }
        public void update()
        {
            this.temperature = weatherForecast.Temperature;
            this.pressure = weatherForecast.Pressure;
            this.humidity = weatherForecast.Humidity;
            display();
        }
        public void display()
        {
            Console.WriteLine("Current temperature:{0}, pressure:{1}, humidity:{2}",temperature,pressure,humidity);
        }
    }
}
