﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab4;
namespace Lab3
{
    internal class Animal: AnimalReaction
    {
        private String name;
        private String kingdom;
        private int legsNumber;
        private int maxAge;
        private string goodReaction;
        private string badReaction;
        private string defaultReaction;
        public Animal() {
            this.name = "";
            this.kingdom = "";
            this.maxAge = 0;
            this.legsNumber = 0;
            this.goodReaction = "";
            this.badReaction = "";
            this.defaultReaction = "";
        }
        public Animal(string name, string kingdom, int legsNumber, int maxAge,
            string goodReaction, string badReaction, string defaultReaction)
        {
            this.name = name;
            this.kingdom = kingdom;
            this.legsNumber = legsNumber;
            this.maxAge = maxAge;
            this.goodReaction = goodReaction;
            this.badReaction = badReaction;
            this.defaultReaction = defaultReaction;
        }
        public String Name { get { return name; } set { name = value; } }
        public String Kingdom { get { return kingdom; } set { kingdom = value; } }
        public int LegsNumber { get { return legsNumber; } set { legsNumber = value; } }
        public int MaxAge { get { return maxAge; } set { maxAge = value; } }

        void AnimalReaction.badReaction()
        {
            Console.WriteLine(badReaction);
        }

        void AnimalReaction.defaultReaction()
        {
            Console.WriteLine(defaultReaction);
        }

        void AnimalReaction.goodReaction()
        {
            Console.WriteLine(goodReaction);
        }
    }
}
