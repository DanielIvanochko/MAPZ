﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3
{
    internal class CatDetachment : InnerSpaceDetachment
    {
        private double area ;
        private String animalType ;
        private String innerSpaceType ;
        private int maxCatsNumber ;
        private List<Animal> animals;
        public CatDetachment()
        {
            this.area = 10;
            this.animalType = "cat";
            this.innerSpaceType = "house";
            this.maxCatsNumber = 40;
            animals = new List<Animal>();
        }
        public string getAnimalType()
        {
            return animalType;
        }

        public double getArea()
        {
            return area;
        }

        public string getInnerSpaceType()
        {
            return innerSpaceType;
        }

        public int getMaxAnimalsNumber()
        {
            return maxCatsNumber;
        }
        public void addAnimal(Animal animal)
        {
            animals.Add(animal);
        }
       
    }
}
