﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3
{
    internal class MyDetachmentFactory:DetachmentFactory
    {
        public InnerSpaceDetachment createInnerSpaceDetachment()
        {
            return new CatDetachment();
        }

        public OuterSpaceDetachment createOuterSpaceDetachment()
        {
            return new HorseDetachment();
        }
    }
}
