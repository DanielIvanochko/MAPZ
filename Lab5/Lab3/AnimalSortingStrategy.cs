﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3
{
    internal interface AnimalSortingStrategy
    {
        public List<Animal> sort(List<Animal> animals);
    }
}
