﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4
{
    internal class AnimalReactionInvoker
    {
        private AnimalReactionCommand animalReactionCommand;
        public AnimalReactionInvoker(AnimalReactionCommand animalReactionCommand)
        {
            this.animalReactionCommand = animalReactionCommand;
        }
        public void setCommand(AnimalReactionCommand animalReactionCommand)
        {
            this.animalReactionCommand = animalReactionCommand;
        }
        public void invoke()
        {
            animalReactionCommand.execute();
        }
    }
}
