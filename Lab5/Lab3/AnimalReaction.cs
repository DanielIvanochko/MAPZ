﻿namespace Lab3
{
    internal interface AnimalReaction
    {
        public void goodReaction();
        public void badReaction();
        public void defaultReaction();
    }
}