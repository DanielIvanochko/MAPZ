﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4
{
    internal class FarmDonationProxy
    {
        private FarmDonation farmDonation;
        public void addMoney(double money)
        {
            if(farmDonation == null)
            {
                farmDonation = new FarmDonation();
            }
            Console.WriteLine("Adding money:{0}", money);
            farmDonation.addMoney(money);
        }
        public void reviewMoney()
        {
            if (farmDonation == null)
            {
                farmDonation = new FarmDonation();
            }
            Console.WriteLine("Reviewing balance...");
            farmDonation.reviewMoney();
        }

        public void spendMoney(double money)
        {
            if (farmDonation == null)
            {
                farmDonation = new FarmDonation();
            }
            Console.WriteLine("Trying to spend money...");
            if (farmDonation.spendMoney(money))
            {
                Console.WriteLine("Money spent!");
            }
            else
            {
                Console.WriteLine("Can't spend money!");
            }
            farmDonation.reviewMoney();

        }
    }
}
