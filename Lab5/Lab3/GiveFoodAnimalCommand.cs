﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab3;
namespace Lab4
{
    internal class GiveFoodAnimalCommand: AnimalReactionCommand
    {
        private AnimalReaction animalReaction;
        public GiveFoodAnimalCommand(AnimalReaction animalReaction)
        {
            this.animalReaction = animalReaction;
        }
        public void execute()
        {
            animalReaction.defaultReaction();
        }
    }
}
