﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4
{
    internal class WeatherForecast : Observable
    {
        private double temperature;
        private double humidity;
        private double pressure;
        List<Observer> observers;
        public WeatherForecast()
        {
            temperature = 0.0;
            humidity = 0.0;
            pressure = 0.0;
            observers = new List<Observer>();
        }
        public void notifyObservers()
        {
            foreach(Observer observer in observers)
            {
                observer.update();
            }
        }

        public void registerObserver(Observer observer)
        {
            observers.Add(observer);
        }

        public void removeObserver(int index)
        {
            observers.RemoveAt(index);
        }
        public void setMeasurments(double temperature, double pressure, double humidity)
        {
            this.temperature = temperature;
            this.humidity = humidity;
            this.pressure = pressure;
            notifyObservers();
        }
        public double Temperature { get { return temperature; } }
        public double Humidity { get { return humidity; } }
        public double Pressure { get { return pressure; } }
    }
}
