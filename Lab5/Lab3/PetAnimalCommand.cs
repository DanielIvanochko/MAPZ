﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab3;
namespace Lab4
{
    internal class PetAnimalCommand: AnimalReactionCommand
    {
        private AnimalReaction animalReaction;
        public PetAnimalCommand(AnimalReaction animalReaction)
        {
            this.animalReaction = animalReaction;
        }
        public void execute()
        {
            animalReaction.goodReaction();
        }
    }
}
