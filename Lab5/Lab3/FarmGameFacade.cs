﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3
{
    internal class FarmGameFacade
    {
        private Farm farm;
        private DetachmentFactory detachmentFactory;
        private List<InnerSpaceDetachment> innerSpaceDetachments;
        private List<OuterSpaceDetachment> outerSpaceDetachments;
        private AnimalCreator animalCreator;
        public FarmGameFacade(DetachmentFactory detachmentFactory, AnimalBuilder animalBuilder)
        {
            this.detachmentFactory = detachmentFactory;
            this.animalCreator = new AnimalCreator(animalBuilder);
            farm = Farm.getInstance();
            this.innerSpaceDetachments = new List<InnerSpaceDetachment>();
            this.outerSpaceDetachments = new List<OuterSpaceDetachment>();
        }
        private void setFarm()
        {
            farm.Name = "Farm";
            farm.Location = "Ukraine";
        }
        public void startGame()
        {
            setFarm();
            innerSpaceDetachments.Add(detachmentFactory.createInnerSpaceDetachment());
            outerSpaceDetachments.Add(detachmentFactory.createOuterSpaceDetachment());
        }
        public void addInnerSpaceDetachment()
        {
            innerSpaceDetachments.Add(detachmentFactory.createInnerSpaceDetachment());   
        }
        public void addOuterSpaceDetachment()
        {
            outerSpaceDetachments.Add(detachmentFactory.createOuterSpaceDetachment());
        }
        public void addAnimalToInnerSpace(int innerSpaceDetachmentIndex)
        {
            animalCreator.construct();
            innerSpaceDetachments[innerSpaceDetachmentIndex].addAnimal(animalCreator.getAnimal());
        }
        public void addAnimalToOuterSpace(int outerSpaceDetachmentIndex)
        {
            animalCreator.construct();
            outerSpaceDetachments[outerSpaceDetachmentIndex].addAnimal(animalCreator.getAnimal());
        }
        public void setAnimalBuilder(AnimalBuilder animalBuilder)
        {
            animalCreator.setAnimalBuilder(animalBuilder);
        }
    }
}
