﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4
{
    internal interface Observable
    {
        void registerObserver(Observer observer);
        void removeObserver(int index);
        void notifyObservers();
    }
}
