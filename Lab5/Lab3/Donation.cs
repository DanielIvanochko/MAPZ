﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4
{
    internal interface Donation
    {
        void addMoney(double money);
        void reviewMoney();
        bool spendMoney(double money);
    }
}
/*
 State pattern:
Weather state: rainy, sun, snow etc.
 Observer pattern:
Weather forecast

 */
