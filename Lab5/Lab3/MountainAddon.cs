﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4
{
    internal class MountainAddon:FieldAddons
    {
        private Field field;
        public MountainAddon(Field field)
        {
            this.field = field;
        }
        public String getFieldType()
        {
            return field.getFieldType();
        }
        public String getFieldDecription() {
            return field.getFieldType() + ", mountains";
        }
    }
}
