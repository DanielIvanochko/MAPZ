﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3
{
    internal class HorseBuilder:AnimalBuilder
    {
        private Animal animal = new Animal();
        public void setName() {
            animal.Name = "horse";
        }
        public void setMaxAge() {
            animal.MaxAge = 30;
        }
        public void setLegsNumber() {
            animal.LegsNumber = 4;
        }
        public void setKingdom() {
            animal.Kingdom = "Mammal";
        }
        public Animal getAnimal() {
            return animal;
        }
    }
}
