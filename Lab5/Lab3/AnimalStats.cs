﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3
{
    internal class AnimalStats
    {
        private AnimalSortingStrategy animalSortingStrategy;
        private List<Animal> animals;
        public AnimalStats(AnimalSortingStrategy animalSortingStrategy)
        {
            this.animalSortingStrategy = animalSortingStrategy;
            this.animals = new List<Animal>();
        }
        public void setSortingStrategy(AnimalSortingStrategy animalSortingStrategy)
        {
            this.animalSortingStrategy = animalSortingStrategy;
        }
        public void sortAnimals()
        {
            animals = animalSortingStrategy.sort(animals);
        }
        public void display()
        {
            foreach(Animal animal in animals)
            {
                Console.WriteLine("Animal with name {0}, max age {1}, kingdom {2}, legs number {3}", animal.Name, animal.MaxAge, animal.Kingdom, animal.LegsNumber);
            }
        }
    }
}
