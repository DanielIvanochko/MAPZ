﻿using System; 
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4
{
    internal class RiverAddon:FieldAddons
    {
        private Field field;
        public RiverAddon(Field field)
        {
            this.field = field;
        }
        public String getFieldType()
        {
            return field.getFieldType();
        }

        public string getFieldDecription()
        {
            return field.getFieldType() + ", river";
        }
    }
}
