﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    class RefOutTestClass
    {
        public void defaultMethod(int a, int b) {
            a = 1;
            b = 2;
        }
        public void refOutMethod(ref int a, out int b, int c) {
            a +=c;
            b = c;
        }
        public void refMethodWithObject(ref Student student) {
            student.SubjectNumber = 10;
        }
        public void defaultMethodWithObject(Student student)
        {
            student.SubjectNumber = 20;
        }
    }
}
