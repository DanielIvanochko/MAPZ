﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
   class Student: Human
   { 
        private string hobby;
        private double avarageMark;
        private bool hasScholarship;
        private bool doesAttendSportSections;
        private int subjectNumber;
        Subject favouriteSubject;
        public Student(string name,int age,string hobby,double avarageMark,
            bool hasScholarship,bool doesAttendSportSections, int subjectNumber):
            base(name,age)
        {
            this.hobby = hobby;
            this.avarageMark = avarageMark;
            this.hasScholarship = hasScholarship;
            this.doesAttendSportSections = doesAttendSportSections;
            this.subjectNumber = subjectNumber;
        }

        public Subject FavouriteSubject
        {
            get { return favouriteSubject; }
            set { this.favouriteSubject = value; }
        }

        public override bool Equals(object? obj)
        {
            if(!(obj is Student))
            {
                return false;
            }
            else
            {
                Student other = (Student)obj;
                return this.hobby == other.hobby && this.avarageMark == other.avarageMark && this.hasScholarship == other.hasScholarship &&
                    this.subjectNumber == other.subjectNumber;
            }
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public override string ToString()
        {
            return String.Format("{0},{1},{2},{4}", this.getName(),this.avarageMark,this.SubjectNumber,this.Age);
        }
        public Student Clone()
        {
            return (Student)this.MemberwiseClone();
        }
        public Student():base()
        {
            this.hobby = "";
            this.avarageMark = 0;
            this.hasScholarship = false;
            this.doesAttendSportSections=false;
            this.subjectNumber=0;
        }
        private string Hobby
        {
            get { return hobby; }
            set { hobby = value; }
        }
        protected double AvarageMark
        {
            get { return avarageMark; }
            set { avarageMark = value; }
        }
        internal bool HasScholarship
        {
            get { return hasScholarship; }
            set { hasScholarship = value;}
        }
        private protected bool DoesAttendSportSections
        {
            get { return doesAttendSportSections;}
            set { doesAttendSportSections = value;}
        }
        protected internal int SubjectNumber
        {
            get { return subjectNumber; }
            set { subjectNumber = value; }
        }
        public override void printOccupation()
        {
            Console.WriteLine("student");
        }
        public void printOccupation(string occupation)
        {
            Console.WriteLine(occupation);
        }
    }
}
