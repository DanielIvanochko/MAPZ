﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    class VariousAccessMethodsClass
    {
        private long currentNumber = 0;
        public void publicIncrease()
        {
            currentNumber++;
        }
        private void privateIncrease()
        {
            currentNumber++;
        }
        protected void protectedIncrease()
        {
            currentNumber++;
        }
    }
}
