﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Reflection;

namespace Lab1
{
    class MethodsInvoker
    {
        VariousAccessMethodsClass variousAccessMethods;
        NestedVariousAccessMethods nestedVariousAccessMethods;
        public enum ClassToTest
        {
            OPENED,
            NESTED
        }
        public MethodsInvoker()
        {
            variousAccessMethods = new VariousAccessMethodsClass();
            nestedVariousAccessMethods = new NestedVariousAccessMethods();
        }

        class NestedVariousAccessMethods
        {
            private long currentValue = 0;
            public void publicNestedIncrease()
            {
                currentValue++;
            }
            private void privateNestedIncrease()
            {
                currentValue++;
            }
            protected void protectedNestedIncrease()
            {
                currentValue++;
            }
        }

        private string getElapsedTime(Stopwatch stopwatch)
        {
            TimeSpan timeSpan = stopwatch.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
             timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds,
             timeSpan.Milliseconds / 10);
            return elapsedTime;
        }
        public void invokePublicMethod(int times,ClassToTest classToTest)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            for(int i = 0;i< times; i++)
            {
                if (classToTest == ClassToTest.OPENED)
                {
                    variousAccessMethods.publicIncrease();
                }
                else
                {
                    nestedVariousAccessMethods.publicNestedIncrease();
                }
            }
            stopwatch.Stop();
            
            Console.WriteLine(getElapsedTime(stopwatch));
        }
        public void invokePrivateMethod(int times,ClassToTest classToTest)
        {
            Stopwatch stopwatch = new Stopwatch();
            MethodInfo methodInfo = null;
            if (classToTest == ClassToTest.OPENED)
            {
                methodInfo = typeof(VariousAccessMethodsClass).GetMethod("privateIncrease", BindingFlags.NonPublic | BindingFlags.Instance);
            }
            else
            {
                methodInfo = typeof(NestedVariousAccessMethods).GetMethod("privateNestedIncrease", BindingFlags.NonPublic | BindingFlags.Instance);
            }
            stopwatch.Start();
            for (int i = 0;i< times; i++)
            {
                if(classToTest == ClassToTest.OPENED)
                {
                    methodInfo.Invoke(variousAccessMethods, null);
                }
                else
                {
                    methodInfo.Invoke(nestedVariousAccessMethods, null);
                }
            }
            stopwatch.Stop();
            Console.WriteLine(getElapsedTime(stopwatch));
        }
        public void invokeProtectedMethod(int times, ClassToTest classToTest)
        {
            Stopwatch stopwatch = new Stopwatch();
            MethodInfo methodInfo = null;
            if (classToTest == ClassToTest.OPENED)
            {
                methodInfo = typeof(VariousAccessMethodsClass).GetMethod("protectedIncrease", BindingFlags.NonPublic | BindingFlags.Instance);
            }
            else
            {
                methodInfo = typeof(NestedVariousAccessMethods).GetMethod("protectedNestedIncrease", BindingFlags.NonPublic | BindingFlags.Instance);
            }
            stopwatch.Start();
            for (int i = 0; i < times; i++)
            {
                if (classToTest == ClassToTest.OPENED)
                {
                    methodInfo.Invoke(variousAccessMethods, null);
                }
                else
                {
                    methodInfo.Invoke(nestedVariousAccessMethods, null);
                }
            }
            stopwatch.Stop();
            Console.WriteLine(getElapsedTime(stopwatch));
        }
    }
}
