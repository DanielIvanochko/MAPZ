﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    class MyThread: Runnable, Joinable
    {
        public void run() {
            Console.WriteLine("Running!");
        }
        public void join() {
            Console.WriteLine("Thread joined!");
        }
    }
}
