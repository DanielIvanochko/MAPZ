﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    abstract class Human : Creature
    {
        class Idea
        {
            private string ideaName;
            public string IdeaName
            {
                get { return ideaName; }
                set { ideaName = value; }
            }
        }
        private string name;
        private int age;
        Idea currentIdea;
        public static int humanNumber;
        static Human(){
            humanNumber = 0;
        }
        Idea CurrentIdea
        {
            get { return currentIdea; }
            set { currentIdea = value; }
        }
        public Human()
        {
            name = "";
            age = 0;
            humanNumber++;
        }
        public Human(string name, int age)
        {
            this.name = name;
            this.age = age;
            humanNumber++;
        }

        public int Age
        {
            get { return age; }
            set { age = value; }
        }
        public string getName()
        {
            return name;
        }
        public abstract void printOccupation();
       
    }
}
