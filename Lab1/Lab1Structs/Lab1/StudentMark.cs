﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    class StudentMark
    {
        private double mark;
        StudentMark(double mark)
        {
            this.mark = mark;
        }
        public static implicit operator double(StudentMark mark)
        {
            return mark.mark;
        }
        public static explicit operator StudentMark(double value)
        {
            return new StudentMark(value);
        }
    }
}
