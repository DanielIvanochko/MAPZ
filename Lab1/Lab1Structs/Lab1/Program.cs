﻿using Lab1;
public class Program
{
    public static void performBooleanOperationsWithEnum(DaysOfWeek firstDay, DaysOfWeek secondDay)
    {
        Console.WriteLine(firstDay ^ secondDay);
        Console.WriteLine(firstDay | secondDay);
        Console.WriteLine(firstDay & secondDay);
       // Console.WriteLine(firstDay && secondDay);
       // Console.WriteLine(firstDay || secondDay);
    }

    public static void testRefOutClass()
    {
        int a = 0;int b = 0;
        RefOutTestClass refOut = new RefOutTestClass();
        refOut.defaultMethod(a, b);
        Console.WriteLine(a + " " + b);
        refOut.refOutMethod(ref a,out b, 2);
        Console.WriteLine(a + " " + b);
        Student student = new Student();
        refOut.refMethodWithObject(ref student);
        Console.WriteLine(student.SubjectNumber);
        refOut.defaultMethodWithObject(student);
        Console.WriteLine(student.SubjectNumber);
    }

    public static void testBoxing()
    {
        int a = 1;
        object b = a;
        Console.WriteLine(b);
        a = (int)b;
        Console.WriteLine(a);
    }

    public static void testEachMethod(int times)
    {
        MethodsInvoker.ClassToTest opened = MethodsInvoker.ClassToTest.OPENED;
        MethodsInvoker.ClassToTest nested = MethodsInvoker.ClassToTest.NESTED;
        MethodsInvoker methodsInvoker = new MethodsInvoker();
        Console.WriteLine("Opened struct:");
        Console.WriteLine("Public method invocation:");
        methodsInvoker.invokePublicMethod(times,opened);
        Console.WriteLine("Private method invocation:");
        methodsInvoker.invokePrivateMethod(times, opened);
        Console.WriteLine("Nested struct:");
        Console.WriteLine("Public method invocation:");
        methodsInvoker.invokePublicMethod(times, nested);
        Console.WriteLine("Private method invocation:");
        methodsInvoker.invokePrivateMethod(times, nested);
    }
    public static void Main()
    {
        testEachMethod(100000000);
    }
}